﻿namespace HZY.Framework.DynamicApiController.Core;

/// <summary>
/// 请求类型
/// </summary>
public enum DynamicApiControllerHttpMethodEnum
{
    /// <summary>
    /// 
    /// </summary>
    POST,
    /// <summary>
    /// 
    /// </summary>
    PUT,
    /// <summary>
    /// 
    /// </summary>
    DELETE,
    /// <summary>
    /// 
    /// </summary>
    GET,
    /// <summary>
    /// 
    /// </summary>
    OPTIONS,
    /// <summary>
    /// 
    /// </summary>
    PATCH
}
