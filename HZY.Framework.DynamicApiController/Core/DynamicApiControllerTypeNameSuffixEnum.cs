﻿namespace HZY.Framework.DynamicApiController.Core;

/// <summary>
/// 动态 api 接口类默认后缀 枚举
/// </summary>
public enum DynamicApiControllerTypeNameSuffixEnum
{
    /// <summary>
    /// 控制器
    /// </summary>
    Controller,
    /// <summary>
    /// 服务
    /// </summary>
    Service,
    /// <summary>
    /// app 服务
    /// </summary>
    AppService,
    /// <summary>
    /// 应用服务
    /// </summary>
    ApplicationService,
}
