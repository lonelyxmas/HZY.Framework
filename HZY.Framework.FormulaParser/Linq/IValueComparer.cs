﻿namespace HZY.Framework.FormulaParser.Linq;

/// <summary>
/// 公开一个比较两个对象的方法。
/// </summary>
/// <remarks>
/// Unlike <see cref="System.Collections.IComparer"/> 该接口允许返回null作为比较结果
/// 用于在不引发异常的情况下无法比较值的情况。
/// </remarks>
public interface IValueComparer
{

    /// <summary>
    /// 比较两个对象并返回一个值，该值指示其中一个对象是否小于、等于或大于另一个对象。
    /// </summary>
    /// <returns>一个带符号的整数，表示x和y的相对值，如果值无法比较，则为null。</returns>
    int? Compare(object x, object y);
}
