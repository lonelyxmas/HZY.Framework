﻿namespace HZY.Framework.FormulaParser.Linq;

/// <summary>
/// 发生lambda表达式解析错误时引发的异常
/// </summary>
public class LambdaParserException : Exception
{

    /// <summary>
    /// lambda表达式
    /// </summary>
    public string Expression { get; private set; }

    /// <summary>
    /// 发生语法错误的分析器位置
    /// </summary>
    public int Index { get; private set; }

    public LambdaParserException(string expr, int idx, string msg)
        : base(string.Format("{0} at {1}: {2}", msg, idx, expr))
    {
        Expression = expr;
        Index = idx;
    }
}
