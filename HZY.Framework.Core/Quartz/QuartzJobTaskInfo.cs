﻿namespace HZY.Framework.Core.Quartz;

/// <summary>
/// 作业任务信息
/// </summary>
public class QuartzJobTaskInfo
{
    /// <summary>
    /// 唯一表示 key
    /// </summary>
    public string Key { get; set; } = null!;

    /// <summary>
    /// 函数信息
    /// </summary>
    public MethodInfo MethodInfo { get; set; } = null!;

    /// <summary>
    /// 作业任务特性信息
    /// </summary>
    public ScheduledAttribute ScheduledAttribute { get; set; } = null!;

    /// <summary>
    /// 对象类型
    /// </summary>
    public Type ClassType { get; set; } = null!;
}
