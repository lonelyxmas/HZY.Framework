﻿namespace HZY.Framework.Core.Quartz;

/// <summary>
/// 任务类型
/// </summary>
public enum QuartzJobTaskType
{
    /// <summary>
    /// 内存任务 , 任务信息存储在内存中
    /// </summary>
    Memory = 1,

    /// <summary>
    /// 内存异步任务 , 任务信息存储在内存中
    /// </summary>
    MemoryAsync = 2,

    /// <summary>
    /// 数据库任务 , 任务信息存储在数据库中 ， 根据 c# 命名空间 + 类名 + 方法名 作为任务名称
    /// </summary>
    Local = 3,

    /// <summary>
    /// 数据库异步任务 , 任务信息存储在数据库中 ， 根据 c# 命名空间 + 类名 + 方法名 作为任务名称
    /// </summary>
    LocalAsync = 4,

    /// <summary>
    /// todo ... , 还未实现 , 用于扫描项目中控制器文件的 接口方法，识别出 api 地址 ，存储在数据库中，作为任务
    /// </summary>
    Http = 5,

    /// <summary>
    /// todo ... , 还未实现 , 用于扫描项目中控制器文件的 接口方法，识别出 api 地址 ，存储在数据库中，作为异步任务
    /// </summary>
    HttpAsync = 6

}
