﻿/*
 * *******************************************************
 *
 * 作者：HZY
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

namespace HZY.Framework.Core.Utils;

/// <summary>
/// 网站 Web 工具
/// </summary>
public static class WebUtil
{
    /// <summary>
    /// 是否 Ajax 请求
    /// </summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    public static bool IsAjaxRequest(this HttpContext httpContext)
    {
        const string key = "X-Requested-With";
        const string value = "XMLHttpRequest";

        var header = httpContext.Request.Headers
            .Where(w => w.Key.ToLower() == key.ToLower())
            .Select(w => w.Value)
            .FirstOrDefault();

        return !string.IsNullOrWhiteSpace(header) && header.ToString() == value;
    }

    /// <summary>
    /// 是否 Json 请求 content-type=application/json
    /// </summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    public static bool IsJsonRequest(this HttpContext httpContext)
    {
        const string key = "content-type";
        const string value = "application/json";

        var header = httpContext.Request.Headers
            .Where(w => w.Key.ToLower() == key.ToLower())
            .Select(w => w.Value)
            .FirstOrDefault();

        return !string.IsNullOrWhiteSpace(header) && header.ToString() == value;
    }

    /// <summary>
    /// 是否为 html 网页请求
    /// </summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    public static bool IsHtmlRequest(this HttpContext httpContext)
    {
        const string key = "accept";
        const string value = "text/html";

        var header = httpContext.Request.Headers
            .Where(w => w.Key.ToLower() == key.ToLower())
            .Select(w => w.Value)
            .FirstOrDefault();

        return !string.IsNullOrWhiteSpace(header) && header.ToString().Contains(value);
    }

    /// <summary>
    /// 通过 httpcontext 下载文件
    /// </summary>
    /// <param name="httpContext"></param>
    /// <param name="fileContents"></param>
    /// <param name="contentType"></param>
    /// <param name="fileDownloadName"></param>
    /// <returns></returns>
    public static void DownLoadFile(this HttpContext httpContext, byte[] fileContents, string contentType, string fileDownloadName)
    {
        //将 Content-Disposition 设置到 Access-Control-Expose-Headers 为白名单
        httpContext.Response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");
        httpContext.Response.ContentType = contentType;
        httpContext.Response.Headers.Add("Content-Disposition", "attachment; filename=" + WebUtility.UrlEncode(fileDownloadName));
        httpContext.Response.BodyWriter.WriteAsync(fileContents);
        httpContext.Response.BodyWriter.FlushAsync();
    }

    /// <summary>
    /// 验证当前上下文响应内容是否是下载文件
    /// </summary>
    /// <param name="httpContext"></param>
    /// <returns></returns>
    public static bool IsDownLoadFile(this HttpContext httpContext)
    {
        return httpContext.Response.Headers["Content-Disposition"].ToString().StartsWith("attachment; filename=");
    }


    #region Cookie操作

    /// <summary>
    /// 写入Cookie
    /// </summary>
    /// <param name="httpContext"></param>
    /// <param name="key">键</param>
    /// <param name="value">值</param>
    /// <param name="expires">过期时长</param>
    public static void SetCookie(this HttpContext httpContext, string key, string value, int? expires = null)
    {
        if (expires == null)
            httpContext.Response.Cookies.Append(key, value);
        else
            httpContext.Response.Cookies.Append(key, value, new CookieOptions()
            {
                Expires = DateTime.Now.AddMinutes(Convert.ToDouble(expires))
            });
    }

    /// <summary>
    /// 读Cookie值
    /// </summary>
    /// <param name="httpContext"></param>
    /// <param name="key">cookies名称</param>
    /// <returns>返回的cookies</returns>
    public static string GetCookie(this HttpContext httpContext, string key)
    {
        return httpContext.Request.Cookies[key];
    }

    /// <summary>
    /// 删除Cookie对象
    /// </summary>
    /// <param name="httpContext"></param>
    /// <param name="key">Cookie对象名称</param>
    public static void RemoveCookie(this HttpContext httpContext, string key)
    {
        httpContext.Response.Cookies.Delete(key);
    }

    #endregion Cookie操作






}
