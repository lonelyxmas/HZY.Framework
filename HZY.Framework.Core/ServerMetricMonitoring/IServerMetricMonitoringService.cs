﻿namespace HZY.Framework.Core.ServerMetricMonitoring;

/// <summary>
/// 服务器指标监控服务
/// </summary>
public interface IServerMetricMonitoringService
{
    /// <summary>
    /// 获取计算机信息
    /// </summary>
    /// <returns></returns>
    RamInfo GetComputerInfo();

    /// <summary>
    /// 获取CPU使用率
    /// </summary>
    /// <returns></returns>
    string GetCpuRate();

    /// <summary>
    /// 获取服务器运行时间
    /// </summary>
    /// <returns></returns>
    string GetRunTime();

    /// <summary>
    /// 获取磁盘信息
    /// </summary>
    /// <returns></returns>
    List<DiskInfo> GetDiskInfos();

}
