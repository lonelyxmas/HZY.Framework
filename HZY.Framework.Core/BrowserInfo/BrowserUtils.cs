﻿namespace HZY.Framework.Core.BrowserInfo;

/// <summary>
/// 浏览器识别 工具类
/// </summary>
public static class BrowserUtils
{
    private static Dictionary<string, BrowserInfo> Browsers = new Dictionary<string, BrowserInfo>()
    {
        {"Edg", BrowserInfo.Edge},
        {"Chrome", BrowserInfo.Chrome},
        {"MSIE", BrowserInfo.IE},
        {"Trident", BrowserInfo.IE},
        {"SogouMobileBrowser", BrowserInfo.Sogou},
        {"MetaSr", BrowserInfo.Sogou},
        {"360 Alitephone Browser", BrowserInfo._360},
        {"Micromessenger", BrowserInfo.Wx},
        {"HuaweiBrowser", BrowserInfo.Huawei},
        {"Baiduboxapp", BrowserInfo.Baidu},
        {"MiuiBrowser", BrowserInfo.Miui},
        {"QQBrowser", BrowserInfo.QQ},
        {"UCBrowser", BrowserInfo.UC},
        {"UBrowser", BrowserInfo.UC},
        {"LeBrowser", BrowserInfo.Le},
        {"TheWorld", BrowserInfo.TheWorld},
        {"Firefox", BrowserInfo.Firefox},
        {"Maxthon", BrowserInfo.Maxthon},
        {"LieBao", BrowserInfo.LieBao},
        {"Quark", BrowserInfo.Quark},
        {"OPR", BrowserInfo.Opera},
        {"Safari", BrowserInfo.Safari}
    };

    public static string ParseBrowser(string userAgentString)
    {
        BrowserInfo browser = BrowserInfo.Unknown;

        if (string.IsNullOrWhiteSpace(userAgentString))
        {
            return browser.Name;
        }

        foreach (var item in Browsers)
        {
            if (userAgentString.Contains(item.Key))
            {
                browser = item.Value;
                break;
            }
        }

        var browserName = browser.Name;

        if (!string.IsNullOrWhiteSpace(browser.Regex))
        {
            var match = Regex.Match(userAgentString, browser.Regex, RegexOptions.IgnoreCase, TimeSpan.FromSeconds(1));
            if (match.Success)
            {
                var version = match.Groups[0].Value.Split(browser.Separator)[1];
                if (browserName == BrowserInfo.IE.Name)
                {
                    version = BrowserInfo.IE.Name + " " + browserName.Split("[.]")[0];
                }

                browserName += version;
            }
        }

        return browserName;
    }
}

public class BrowserInfo
{

    public static readonly BrowserInfo IE = new BrowserInfo("Internet Explorer", "(MSIE \\d+\\.\\d+)|(rv:\\d+\\.\\d+)", " |:");
    public static readonly BrowserInfo Edge = new BrowserInfo("Edge", "Edg\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Chrome = new BrowserInfo("Chrome", "Chrome\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Firefox = new BrowserInfo("Firefox", "Firefox\\/\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Safari = new BrowserInfo("Safari", "Safari\\/\\d+\\.\\d+(\\.\\d+)?", "/");
    public static readonly BrowserInfo Opera = new BrowserInfo("Opera", "OPR\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Sogou = new BrowserInfo("搜狗浏览器", "SogouMobileBrowser\\/\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Wx = new BrowserInfo("微信浏览器");
    public static readonly BrowserInfo Baidu = new BrowserInfo("百度浏览器", "baiduboxapp\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo QQ = new BrowserInfo("QQ浏览器", "QQBrowser\\/\\d+\\.\\d+(\\.\\d+\\.\\d+)?", "/");
    public static readonly BrowserInfo _360 = new BrowserInfo("360极速浏览器", "360 Alitephone Browser \\(\\d+\\.\\d+\\.\\d+\\.\\d+\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo UC = new BrowserInfo("UC浏览器", "UC?Browser\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Maxthon = new BrowserInfo("遨游浏览器", "Maxthon\\/\\d", "/");
    public static readonly BrowserInfo LieBao = new BrowserInfo("猎豹浏览器", "LieBaoFast\\/\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo TheWorld = new BrowserInfo("世界之窗浏览器", "TheWorld \\d+", "/");
    public static readonly BrowserInfo Quark = new BrowserInfo("夸克", "Quark\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Huawei = new BrowserInfo("华为浏览器", "HuaweiBrowser\\/\\d+\\.\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Le = new BrowserInfo("绿茶浏览器", "LeBrowser\\/\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Miui = new BrowserInfo("Miui浏览器", "MiuiBrowser\\/\\d+\\.\\d+\\.\\d+", "/");
    public static readonly BrowserInfo Unknown = new BrowserInfo("Other");

    public string Name { get; set; }

    public string Regex { get; set; }

    public string Separator { get; set; }

    public BrowserInfo(string name) : this(name, "", "")
    {
    }

    public BrowserInfo(string name, string regex, string separator)
    {
        Name = name;
        Regex = regex;
        Separator = separator;
    }
}
