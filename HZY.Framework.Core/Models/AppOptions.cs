﻿namespace HZY.Framework.Core.Models;

/// <summary>
/// app 配置
/// </summary>
public class AppOptions
{
    /// <summary>
    /// WebApplicationBuilder 构建器，可以配置服务
    /// </summary>
    public Action<WebApplicationBuilder>? WebApplicationBuilderAction { get; set; }

    /// <summary>
    /// WebApplicationAction
    /// </summary>
    public Action<WebApplication>? WebApplicationAction { get; set; }
}