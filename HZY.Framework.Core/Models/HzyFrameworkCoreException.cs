﻿namespace HZY.Framework.Core.Models;

/// <summary>
/// 框架核心异常
/// </summary>
[Serializable]
public class HzyFrameworkCoreException : Exception, ISerializable
{
    /// <summary>
    /// 框架核心异常
    /// </summary>
    public HzyFrameworkCoreException()
    {
    }

    /// <summary>
    /// 框架核心异常
    /// </summary>
    /// <param name="message"></param>
    public HzyFrameworkCoreException(string? message) : base(message)
    {
    }

    /// <summary>
    /// 框架核心异常
    /// </summary>
    /// <param name="message"></param>
    /// <param name="innerException"></param>
    public HzyFrameworkCoreException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    /// <summary>
    /// 框架核心异常
    /// </summary>
    /// <param name="info"></param>
    /// <param name="context"></param>
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("This API supports obsolete formatter-based serialization. It should not be called or extended by application code.", DiagnosticId = "SYSLIB0051", UrlFormat = "https://aka.ms/dotnet-warnings/{0}")]
    protected HzyFrameworkCoreException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

}
