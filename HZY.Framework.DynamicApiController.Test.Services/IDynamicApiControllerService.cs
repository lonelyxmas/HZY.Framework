﻿namespace HZY.Framework.DynamicApiController.Test.Services
{
    public class IDynamicApiControllerService : IDynamicApiController
    {

        public string GetAsync(string name)
        {
            return "GetAsync Name=" + name;
        }

        public string PostAsync()
        {
            return "PostAsync";
        }

        public string DeleteAsync()
        {
            return "DeleteAsync";
        }

        public string PUT()
        {
            return "PUT";
        }

        public string PATCH()
        {
            return "PATCH";
        }

        public string OPTIONS()
        {
            return "OPTIONS";
        }




    }
}
