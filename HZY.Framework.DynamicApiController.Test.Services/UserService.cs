﻿using Microsoft.AspNetCore.Mvc;

namespace HZY.Framework.DynamicApiController.Test.Services
{
    [Route("/api/[controller]/[action]")]
    //[Route("/api/[controller]")]
    //[Route("/api")]
    //[ApiExplorerSettings(GroupName = "123")]
    [ApiController]
    public class UserService
    {
        public string GetAsync(string name)
        {
            return "GetAsync";
        }

        public string PostAsync()
        {
            return "PostAsync";
        }

        public string DeleteAsync()
        {
            return "DeleteAsync";
        }

        public string PUT()
        {
            return "PUT";
        }

        public string PATCH()
        {
            return "PATCH";
        }

        public string OPTIONS()
        {
            return "OPTIONS";
        }

        [NonAction]
        public string BadAsync()
        {
            return "111";
        }

        [HttpGet("{id}")]
        public string Test([FromRoute] string id)
        {
            return "Test id=" + id;
        }

        [HttpGet("{code}", Name = "Test6611111111")]
        public string Test66([FromRoute] string code)
        {
            return "Test66 Code=" + code;
        }

        [HttpGet("{code}")]
        public string Test333([FromRoute] string code)
        {
            return "Test333 Code=" + code;
        }

        [HttpGet]
        public string Test999(string code)
        {
            return "Test999 Code=" + code;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public string Test100(string code)
        {
            return "Test999 Code=" + code;
        }

        //[HttpGet]
        //public string GetWeatherForecast()
        //{
        //    return "GetWeatherForecast";
        //}

    }
}