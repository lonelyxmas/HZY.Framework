﻿using HZY.Framework.Dict.SourceGenerator.Test;

namespace HZY.Api.Demo;

public partial class UserInfo
{

    [Dict]
    public string Name { get; set; }

}
