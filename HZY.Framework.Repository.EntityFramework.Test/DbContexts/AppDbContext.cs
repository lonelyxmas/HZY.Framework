﻿using HZY.Framework.Repository.EntityFramework.Attributes;
using HZY.Framework.Repository.EntityFramework.Interceptor;
using HZY.Framework.Repository.EntityFramework.Models;
using HZY.Framework.Repository.EntityFramework.Models.Enums;
using HZY.Framework.Repository.EntityFramework.Repositories;
using HZY.Framework.Repository.EntityFramework.Repositories.Impl;
using HZY.Framework.Repository.EntityFramework.Test.Models;
using Microsoft.EntityFrameworkCore;

namespace HZY.Framework.Repository.EntityFramework.Test.DbContexts
{
    //[DbContextConfig("HZY.Framework.Repository.EntityFramework.Test.Models.*")]
    public class AppDbContext : DbContext, IBaseDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            UnitOfWork = new UnitOfWorkImpl<AppDbContext>(this);
        }

        public IUnitOfWork UnitOfWork { get; }

        DbSet<SysFunction> SysFunction { get; set; }

        public RepositoryOptions GetRepositoryOptions()
        {
            return new RepositoryOptions()
            {
                ConnectionString = @"Server=127.0.0.1; port=3306; Database=hzy_admin_mysql_20230227; uid=root; pwd=123456; Convert Zero Datetime=False;sslMode=None",
                IsMonitorEFCore = false,
                DefaultDatabaseType = DefaultDatabaseType.MySql,
                SlaveConnectionStrings = [
                @"Server=127.0.0.1; port=3306; Database=hzy_admin_mysql_20230227_slave; uid=root; pwd=123456; Convert Zero Datetime=False;sslMode=None"
                ]
            };
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var options = this.GetRepositoryOptions();
            optionsBuilder.UseSqlServer(options.ConnectionString);
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));
            optionsBuilder.AddInterceptors(new ReadWriteCommandInterceptor());

            base.OnConfiguring(optionsBuilder);
        }

    }

    [DbContextConfig("HZY.Framework.Repository.EntityFramework.Test.Models.*")]
    public class AppDbContext1 : DbContext, IBaseDbContext
    {
        public IUnitOfWork UnitOfWork { get; }

        public AppDbContext1(DbContextOptions<AppDbContext1> options) : base(options)
        {
            UnitOfWork = new UnitOfWorkImpl<AppDbContext1>(this);
        }

        DbSet<SysFunction> SysFunction { get; set; }

        public RepositoryOptions GetRepositoryOptions()
        {
            return new RepositoryOptions()
            {
                ConnectionString = @"Server=127.0.0.1; port=3306; Database=hzy_admin_mysql_20230227; uid=root; pwd=123456; Convert Zero Datetime=False;sslMode=None",
                IsMonitorEFCore = false,
                DefaultDatabaseType = DefaultDatabaseType.MySql,
                SlaveConnectionStrings = [
                @"Server=127.0.0.1; port=3306; Database=hzy_admin_mysql_20230227_slave; uid=root; pwd=123456; Convert Zero Datetime=False;sslMode=None"
                ]
            };
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var options = this.GetRepositoryOptions();
            optionsBuilder.UseMySql(options.ConnectionString, MySqlServerVersion.LatestSupportedServerVersion);
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));
            optionsBuilder.AddInterceptors(new ReadWriteCommandInterceptor());

            base.OnConfiguring(optionsBuilder);
        }

    }


}
