using HZY.Framework.Repository.EntityFramework;
using HZY.Framework.Repository.EntityFramework.Models.Enums;
using HZY.Framework.Repository.EntityFramework.Test;
using HZY.Framework.Repository.EntityFramework.Test.DbContexts;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddControllersAsServices();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//取消域验证
builder.Host.UseDefaultServiceProvider(options => { options.ValidateScopes = false; });


#region 注册

// 注册上下文
builder.Services.AddDbContextFactory<AppDbContext>((serviceProvider, options) =>
   {
   });

// 注册上下文
builder.Services.AddDbContextFactory<AppDbContext1>((serviceProvider, options) =>
{
    //options.UseQueryTrackingBehavior(Microsoft.EntityFrameworkCore.QueryTrackingBehavior.NoTracking);
});
// 注册仓储
builder.Services.AddEntityFrameworkRepositories<AppDbContext1>(new HZY.Framework.Repository.EntityFramework.Models.RepositoryOptions
{
    ConnectionString = @"Server=127.0.0.1; port=3306; Database=hzy_admin_mysql_20230227; uid=root; pwd=123456; Convert Zero Datetime=False;sslMode=None",
    IsMonitorEFCore = false,
    DefaultDatabaseType = DefaultDatabaseType.MySql,
    SlaveConnectionStrings = [
        @"Server=127.0.0.1; port=3306; Database=hzy_admin_mysql_20230227_slave; uid=root; pwd=123456; Convert Zero Datetime=False;sslMode=None"
        ]
}, (options) =>
{

}, (freesqlOptions) =>
{
    freesqlOptions.FreeSqlAuditAopList?.Add(new FreeSqlAuditAop());
    freesqlOptions.FreeSqlAction = (freeSql) =>
    {
        freeSql.Aop.CurdAfter += (object? sender, FreeSql.Aop.CurdAfterEventArgs curdAfter) =>
        {
            //var stringBuilder = new StringBuilder();
            //stringBuilder.Append($"\r\n====[FreeSql 开始 耗时: {curdAfter.ElapsedMilliseconds} ms]=========");
            //stringBuilder.Append($"\r\n{curdAfter.Sql}");
            //stringBuilder.Append($"\r\n====[FreeSql 结束 线程Id:{Environment.CurrentManagedThreadId}]=========");
            //LogUtil.Log.Warning(stringBuilder.ToString());
        };

        freeSql.Aop.ConfigEntityProperty += (s, e) =>
        {
            if (e.Property.PropertyType.IsEnum)
                e.ModifyResult.MapType = typeof(int);
        };
    };
});

#endregion

//builder.Services.AddScoped<TestService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//EntityFrameworkRepositoriesExtensions.UseEntityFrameworkRepositories(typeof(AppDbContext1));

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
