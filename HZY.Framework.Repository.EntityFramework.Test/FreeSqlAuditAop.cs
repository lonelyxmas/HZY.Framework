﻿using HZY.Framework.Repository.EntityFramework.Interceptor;

namespace HZY.Framework.Repository.EntityFramework.Test;

/// <summary>
/// 审计 保存拦截器
/// </summary>
public class FreeSqlAuditAop : AbstractFreeSqlAuditAop
{
    /// <summary>
    /// 获取当前用户 id
    /// </summary>
    /// <returns></returns>
    protected override string? GetCurrentUserId()
    {
        return Guid.NewGuid().ToString();
    }

    /// <summary>
    /// 获取雪花id
    /// </summary>
    /// <returns></returns>
    protected override long GetSnowflakeId()
    {
        return 0L;
    }
}