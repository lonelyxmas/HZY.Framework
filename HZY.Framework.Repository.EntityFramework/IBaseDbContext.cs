﻿using IUnitOfWork = HZY.Framework.Repository.EntityFramework.Repositories.IUnitOfWork;

namespace HZY.Framework.Repository.EntityFramework;

/// <summary>
/// dbContext 接口 约束
/// </summary>
public interface IBaseDbContext
{

    /// <summary>
    /// 工作单元
    /// </summary>
    public IUnitOfWork UnitOfWork { get; }

    /// <summary>
    /// 获取仓储配置
    /// </summary>
    /// <returns></returns>
    public RepositoryOptions GetRepositoryOptions();

}
