﻿using DbContext = Microsoft.EntityFrameworkCore.DbContext;
using DbContextOptionsBuilder = Microsoft.EntityFrameworkCore.DbContextOptionsBuilder;

namespace HZY.Framework.Repository.EntityFramework;

/// <summary>
/// EntityFrameworkRepositories 工具类
/// </summary>
public static class RepositoryEntityFrameworkExtensions
{
    /// <summary>
    /// 实体 和 dbContext 集合
    /// </summary>
    private static readonly List<DbContextAndEntityInfo> DbContextAndEntityInfos = new();

    /// <summary>
    /// 审计对象
    /// </summary>
    private static readonly List<AuditOptions> _auditOptions = new();

    /// <summary>
    /// 审计对象
    /// </summary>
    public static List<AuditOptions> AuditOptions => _auditOptions;

    /// <summary>
    /// FreeSql 对象
    /// </summary>
    public static Dictionary<Type, IFreeSql> FreeSqlOrmList = new();

    static RepositoryEntityFrameworkExtensions()
    {
    }

    /// <summary>
    /// 缓存 dbContext 类型
    /// </summary>
    /// <param name="entityType"></param>
    /// <param name="dbContextType"></param>
    public static void AddDbContextAndEntityInfo(Type entityType, Type dbContextType)
    {
        //验证是否已经存在
        if (DbContextAndEntityInfos.Any(a =>
                a.EntityType.FullName == entityType.FullName && a.DbContextType.FullName == dbContextType.FullName))
            return;

        DbContextAndEntityInfos.Add(new DbContextAndEntityInfo(entityType, dbContextType));
    }

    /// <summary>
    /// 获取缓存的 dbContext 类型
    /// </summary>
    /// <returns></returns>
    public static List<DbContextAndEntityInfo> GetDbContextAndEntityInfoAll() => DbContextAndEntityInfos;

    /// <summary>
    /// 获取所有的 dbContext
    /// </summary>
    /// <returns></returns>
    public static List<Type> GetDbContextTypeAll() =>
        DbContextAndEntityInfos.Select(s => s.DbContextType).Distinct().ToList();

    #region EntityFrameworkRepositories

    /// <summary>
    /// 添加监控 EntityFramework
    /// </summary>
    /// <param name="dbContextOptionsBuilder"></param>
    /// <param name="isMonitor"></param>
    /// <returns></returns>
    public static DbContextOptionsBuilder AddEntityFrameworkMonitor(
    this DbContextOptionsBuilder dbContextOptionsBuilder, bool isMonitor = false)
    {
        dbContextOptionsBuilder.AddInterceptors(new ShardingDbCommandInterceptor());

        //注册监控程序
        if (isMonitor)
        {
            dbContextOptionsBuilder.AddInterceptors(new MonitorDbConnectionInterceptor());
            dbContextOptionsBuilder.AddInterceptors(new MonitorDbCommandInterceptor());
            dbContextOptionsBuilder.AddInterceptors(new MonitorDbTransactionInterceptor());
        }

        return dbContextOptionsBuilder;
    }

    /// <summary>
    /// 注册 EntityFrameworkRepositories
    /// 主要用于注册动态仓储
    /// </summary>
    /// <param name="services"></param>
    /// <param name="repositoryOptions"></param>
    /// <param name="auditOptionsCallBack">审计信息配置</param>
    /// <param name="freeSqlOptionsCallBack">freesql 构建回调</param>
    /// <param name="serviceLifetime"></param>
    /// <returns></returns>
    public static IServiceCollection AddEntityFrameworkRepositories<TDbContext>(
        this IServiceCollection services,
        RepositoryOptions repositoryOptions,
        Action<List<AuditOptions>>? auditOptionsCallBack = null,
        Action<FreeSqlOptions>? freeSqlOptionsCallBack = null,
        ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
        where TDbContext : DbContext
    {
        Type dbContextType = typeof(TDbContext);

        List<AuditOptions> auditOptions = new();

        #region 添加系统默认标准审计字段

        // 审计字段 - 标准版本
        auditOptions!.Add(new AuditOptions()
        {
            CreationTimeFieldName = nameof(ICreateAndUpdateEntity.CreationTime),
            CreatorUserIdFieldName = nameof(ICreateAndUpdateEntity.CreatorUserId),
            LastModificationTimeFieldName = nameof(ICreateAndUpdateEntity.LastModificationTime),
            LastModifierUserIdFieldName = nameof(ICreateAndUpdateEntity.LastModifierUserId),
            DeleterUserIdFieldName = nameof(IDeleteEntity.DeleterUserId),
            DeletionTimeFieldName = nameof(IDeleteEntity.DeletionTime),
            IsDeletedFieldName = nameof(IDeleteEntity.IsDeleted),
        });
        // 审计字段 - 标准版本v2
        auditOptions.Add(new AuditOptions()
        {
            CreationTimeFieldName = nameof(ICreateAndUpdateEntityV2.CreateTime),
            CreatorUserIdFieldName = nameof(ICreateAndUpdateEntityV2.CreateBy),
            LastModificationTimeFieldName = nameof(ICreateAndUpdateEntityV2.UpdateTime),
            LastModifierUserIdFieldName = nameof(ICreateAndUpdateEntityV2.UpdateBy),
            DeletionTimeFieldName = nameof(IDeleteEntityV2.DeleteTime),
            DeleterUserIdFieldName = nameof(IDeleteEntityV2.DeleteBy),
            IsDeletedFieldName = nameof(IDeleteEntityV2.DeleteFlag),
        });

        auditOptionsCallBack?.Invoke(auditOptions);
        _auditOptions.AddRange(auditOptions);
        #endregion

        var dbContextConfigAttribute = dbContextType.GetCustomAttribute<DbContextConfigAttribute>()!;

        UseEntityFrameworkRepositories(dbContextType);

        // 如果上下文没有配置 DbContextConfigAttribute 则使用默认的扫描方式 DbSet<> 注册
        if (dbContextConfigAttribute != null)
        {
            // 将实体存储起来
            var modelTypes = dbContextConfigAttribute.GetModelTypes(dbContextType);
            foreach (var item in modelTypes)
            {
                AddDbContextAndEntityInfo(item, dbContextType);
            }
        }

        // 将实体与仓储接口注入到 ioc
        GetDbContextAndEntityInfoAll()
            .Where(w => w.DbContextType.FullName == dbContextType.FullName)
            .ToList()
            .ForEach(item =>
            {
                var interfaceType = typeof(IRepository<>).MakeGenericType(item.EntityType);
                var implType = typeof(RepositoryImpl<,>).MakeGenericType(item.EntityType, dbContextType);

                switch (serviceLifetime)
                {
                    case ServiceLifetime.Transient:
                        services.AddTransient(interfaceType, implType);
                        break;
                    case ServiceLifetime.Singleton:
                        services.AddSingleton(interfaceType, implType);
                        break;
                    default:
                        services.AddScoped(interfaceType, implType);
                        break;
                }
            });

        #region freesql

        if (freeSqlOptionsCallBack is not null)
        {
            FreeSqlOptions freeSqlOptions = new();
            freeSqlOptionsCallBack.Invoke(freeSqlOptions);

            // 处理 FreeSql
            if (!FreeSqlOrmList.ContainsKey(dbContextType))
            {
                var dataType = FreeSql.DataType.SqlServer;

                if (repositoryOptions.DefaultDatabaseType == DefaultDatabaseType.SqlServer)
                {
                    dataType = FreeSql.DataType.SqlServer;
                }
                else if (repositoryOptions.DefaultDatabaseType == DefaultDatabaseType.MySql)
                {
                    dataType = FreeSql.DataType.MySql;
                }
                else if (repositoryOptions.DefaultDatabaseType == DefaultDatabaseType.PostgreSql)
                {
                    dataType = FreeSql.DataType.PostgreSQL;
                }
                else if (repositoryOptions.DefaultDatabaseType == DefaultDatabaseType.Oracle)
                {
                    dataType = FreeSql.DataType.Oracle;
                }
                //else if (repositoryOptions.DefaultDatabaseType == DefaultDatabaseType.Sqlite)
                //{
                //    dataType = FreeSql.DataType.Sqlite;
                //}

                var freeSqlBuilder = new FreeSqlBuilder()
                    .UseConnectionString(dataType, repositoryOptions.ConnectionString)
                    .UseAutoSyncStructure(false) //自动迁移实体的结构到数据库
                                                 //.UseMonitorCommand(cmd => Console.WriteLine($"FreeSql：{cmd.CommandText}"))//监听SQL语句
                    .UseNameConvert(NameConvertType.PascalCaseToUnderscoreWithLower)
                    ;

                freeSqlOptions.FreeSqlBuilderAction?.Invoke(freeSqlBuilder);

                var freeSql = freeSqlBuilder.Build(); //请务必定义成 Singleton 单例模式

                // Aop
                //freeSql.Aop.CurdAfter += (s, curdAfter) =>
                //{
                //    //if (curdAfter.ElapsedMilliseconds > 1000)
                //    {
                //        var stringBuilder = new StringBuilder();
                //        stringBuilder.Append($"\r\n====[FreeSql 开始 耗时: {curdAfter.ElapsedMilliseconds} ms]=========");
                //        stringBuilder.Append($"\r\n{curdAfter.Sql}");
                //        stringBuilder.Append($"\r\n====[FreeSql 结束 线程Id:{Environment.CurrentManagedThreadId}]=========");
                //        Console.WriteLine(stringBuilder);
                //        //LogUtil.Log.Warning(stringBuilder.ToString());
                //    }
                //};
                // Aop 审计
                freeSql.Aop.AuditValue += (s, auditInfo) =>
                {
                    if (freeSqlOptions.FreeSqlAuditAopList is not null && freeSqlOptions.FreeSqlAuditAopList.Count > 0)
                    {
                        freeSqlOptions.FreeSqlAuditAopList.ForEach(a =>
                        {
                            a.OnAudit(auditInfo, s);
                        });
                    }
                };

                freeSql.Aop.ConfigEntity += (s, e) =>
                {
                    var tableAttribute = e.EntityType.GetCustomAttribute<TableAttribute>();
                    if (tableAttribute != null)
                    {
                        e.ModifyResult.Name = tableAttribute.Name;
                        return;
                    }

                    var entityDescriptionAttribute = e.EntityType.GetCustomAttribute<EntityDescriptionAttribute>();
                    if (entityDescriptionAttribute == null) return;

                    if (entityDescriptionAttribute.FieldIgnored)
                    {
                        e.ModifyResult.Name = e.EntityType.Name;
                        return;
                    }

                    e.ModifyResult.Name = EntityFrameworkRepositoriesUtil.GetNameByNameRuleType(e.EntityType.Name, entityDescriptionAttribute.NameRuleType); //表名
                };

                freeSql.Aop.ConfigEntityProperty += (s, e) =>
                {
                    // 处理枚举类型
                    if (e.Property.PropertyType.IsEnum)
                    {
                        e.ModifyResult.MapType = typeof(int);
                    }

                    var columnAttribute = e.EntityType.GetCustomAttribute<ColumnAttribute>();
                    if (columnAttribute != null && !string.IsNullOrWhiteSpace(columnAttribute.Name))
                    {
                        e.ModifyResult.Name = columnAttribute.Name;
                        return;
                    }

                    var entityDescriptionAttribute = e.EntityType.GetCustomAttribute<EntityDescriptionAttribute>();

                    if (entityDescriptionAttribute == null) return;

                    if (entityDescriptionAttribute.FieldIgnored)
                    {
                        e.ModifyResult.Name = e.Property.Name;
                        return;
                    }

                    e.ModifyResult.Name = EntityFrameworkRepositoriesUtil.GetNameByNameRuleType(e.Property.Name, entityDescriptionAttribute.NameRuleType); // 字段名
                };

                freeSqlOptions.FreeSqlAction?.Invoke(freeSql);

                FreeSqlOrmList[dbContextType] = freeSql; //请务必定义成 Singleton 单例模式
            }
        }

        #endregion

        return services;
    }

    /// <summary>
    /// 使用 EntityFrameworkRepositories
    /// </summary>
    /// <param name="dbContextTypes">数据上下文类型</param>
    /// <returns></returns>
    private static void UseEntityFrameworkRepositories(params Type[] dbContextTypes)
    {
        foreach (var item in dbContextTypes)
        {
            //扫描类型下面的 dbSet model
            var propertyInfos =
                item.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            var dbSets = propertyInfos.Where(w => w.PropertyType.Name == "DbSet`1");

            foreach (var dbSet in dbSets)
            {
                if (dbSet.PropertyType.GenericTypeArguments.Length <= 0) continue;

                var model = dbSet.PropertyType.GenericTypeArguments[0];
                AddDbContextAndEntityInfo(model, item);
            }
        }
    }

    #endregion
}