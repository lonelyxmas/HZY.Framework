﻿/*
 * *******************************************************
 *
 * 作者：hzy
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace HZY.Framework.Repository.EntityFramework.Repositories;

/// <summary>
/// 仓储基础
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IRepositoryCore<T> //: IDisposable, IAsyncDisposable
    where T : class, new()
{
    /// <summary>
    /// 设置 Attach
    /// </summary>
    /// <param name="model"></param>
    /// <param name="entityState"></param>
    void SetEntityState(T model, EntityState entityState);

    /// <summary>
    /// 取消了实体对象的追踪操作 需要调用此函数 才能进行对实体数据库操作
    /// <para>
    /// 用于取消旧实体追踪缓存 防止出现 id 重复问题
    /// </para>
    ///  
    /// <para>
    /// 此函数解决的问题可以看此案例： https://blog.51cto.com/u_15064638/4401901
    /// </para>
    /// 
    /// </summary>
    /// <param name="detachedWhere"></param>
    void DetachWhenExist(Func<T, bool> detachedWhere);

    /// <summary>
    /// 传入一个 值 获取实体 key 的 表达式
    /// </summary>
    /// <param name="value"></param>
    /// <returns> w => w.Id = 1 </returns>
    Expression<Func<T, bool>> GetKeyExpression(object value);

    /// <summary>
    /// 获取数据上下文
    /// </summary>
    /// <typeparam name="TDbContextResult"></typeparam>
    /// <returns></returns>
    TDbContextResult? GetContext<TDbContextResult>() where TDbContextResult : DbContext;

    /// <summary>
    /// 获取数据上下文 基础对象 DbContext
    /// </summary>
    /// <returns></returns>
    DbContext? GetContext() => default;

    /// <summary>
    /// 工作单元
    /// </summary>
    IUnitOfWork UnitOfWork => default!;
}