﻿/*
 * *******************************************************
 *
 * 作者：hzy
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

namespace HZY.Framework.Repository.EntityFramework.Repositories;

/// <summary>
/// 工作单元
/// </summary>
public interface IUnitOfWork //: IDisposable, IAsyncDisposable
{
    /// <summary>
    /// 获取保存状态
    /// </summary>
    /// <returns></returns>
    bool GetDelaySaveState();

    /// <summary>
    /// 设置保存状态
    /// </summary>
    /// <param name="saveSate"></param>
    void SetDelaySaveState(bool saveSate);

    /// <summary>
    /// 打开延迟提交
    /// </summary>
    void CommitDelayStart();

    /// <summary>
    /// 延迟提交结束
    /// </summary>
    /// <returns></returns>
    int CommitDelayEnd(int retryCount = 100);

    /// <summary>
    /// 延迟提交结束
    /// </summary>
    /// <returns></returns>
    Task<int> CommitDelayEndAsync(int retryCount = 100);

    /// <summary>
    /// 开始事务
    /// </summary>
    /// <returns></returns>
    IDbContextTransaction BeginTransaction();

    /// <summary>
    /// 开始事务
    /// </summary>
    /// <returns></returns>
    Task<IDbContextTransaction> BeginTransactionAsync();

    /// <summary>
    /// 获取当前上下文事务
    /// </summary>
    IDbContextTransaction? CurrentDbContextTransaction => default;

    /// <summary>
    /// 获取当前事务
    /// </summary>
    IDbTransaction? CurrentDbTransaction => default;

    /// <summary>
    /// freeSql 对象
    /// </summary>
    IFreeSql? FreeSqlOrm => default!;

    /// <summary>
    /// 获取当前 事务 根据 IDbContextTransaction 事务
    /// </summary>
    /// <param name="dbContextTransaction"></param>
    /// <returns></returns>
    IDbTransaction? GetDbTransaction(IDbContextTransaction dbContextTransaction);

    /// <summary>
    /// 获取 dbset 对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Microsoft.EntityFrameworkCore.DbSet<T> DbSet<T>() where T : class, new() => default!;

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <returns></returns>
    int SaveChanges();

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <param name="acceptAllChangesOnSuccess"></param>
    /// <returns></returns>
    int SaveChanges(bool acceptAllChangesOnSuccess);

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <returns></returns>
    Task<int> SaveChangesAsync();

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <param name="acceptAllChangesOnSuccess"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
        CancellationToken cancellationToken = new CancellationToken());

    #region 原生 sql 操作

    /// <summary>
    /// 查询根据sql语句
    /// </summary>
    /// <returns> IQueryable </returns>
    IQueryable<T> QueryableBySql<T>(string sql, params object[] parameters) where T : class, new();

    /// <summary>
    /// 根据 sql 查询表格
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    DataTable? QueryDataTableBySql(string sql, params object[] parameters);

    /// <summary>
    /// 根据 sql 查询表格
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    Task<DataTable?> QueryDataTableBySqlAsync(string sql, params object[] parameters);

    /// <summary>
    /// 根据 sql 查询字典集合
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    List<Dictionary<string, object?>>? QueryDicBySql(string sql, params object[] parameters);

    /// <summary>
    /// 根据 sql 查询字典集合
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    Task<List<Dictionary<string, object?>>?> QueryDicBySqlAsync(string sql, params object[] parameters);

    /// <summary>
    /// 查询根据sql语句
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    List<T>? QueryBySql<T>(string sql, params object[] parameters) where T : class, new();

    /// <summary>
    /// 查询根据sql语句
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    Task<List<T>?> QueryBySqlAsync<T>(string sql, params object[] parameters) where T : class, new();

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    object? QuerySingleBySql(string sql, params object[] parameters);

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    Task<object?> QuerySingleBySqlAsync(string sql, params object[] parameters);

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    TResult QuerySingleBySql<TResult>(string sql, params object[] parameters)
        where TResult : struct;

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    Task<TResult> QuerySingleBySqlAsync<TResult>(string sql, params object[] parameters)
        where TResult : struct;

    #endregion
}