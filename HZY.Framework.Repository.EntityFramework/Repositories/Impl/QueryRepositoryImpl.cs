﻿/*
 * *******************************************************
 *
 * 作者：hzy
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace HZY.Framework.Repository.EntityFramework.Repositories.Impl;

/// <summary>
/// 基础仓储 查询 实现
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="TDbContext"></typeparam>
public abstract class QueryRepositoryImpl<T, TDbContext> : RepositoryCoreImpl<T, TDbContext>, IQueryRepository<T>
    where T : class, new()
    where TDbContext : DbContext
{
    /// <summary>
    /// 数据库结构操作对象
    /// </summary>
    private readonly IDatabaseSchema? _databaseSchema;

    /// <summary>
    /// 基础仓储 查询 实现
    /// </summary>
    /// <param name="dbContext"></param>
    /// <param name="filter"></param>
    protected QueryRepositoryImpl(TDbContext dbContext, Expression<Func<T, bool>>? filter = null)
        : base(dbContext, filter)
    {
        if (Context.Database.IsSqlServer())
        {
            _databaseSchema = new SqlServerDatabaseSchemaImpl(Context);
        }

        if (Context.Database.IsMySql())
        {
            _databaseSchema = new MySqlDatabaseSchemaImpl(Context);
        }

        if (Context.Database.IsNpgsql())
        {
            _databaseSchema = new NPgsqlDatabaseSchemaImpl(Context);
        }

        if (Context.Database.IsOracle())
        {
            _databaseSchema = new OracleDatabaseSchemaImpl(Context);
        }
    }

    ///// <summary>
    ///// 资源释放
    ///// </summary>
    //protected override void DisposeAll()
    //{
    //    if (_databaseSchema != null)
    //    {
    //        _databaseSchema.Dispose();
    //    }

    //    base.DisposeAll();
    //}

    ///// <summary>
    ///// 资源释放
    ///// </summary>
    //protected override async Task DisposeAllAsync()
    //{
    //    if (_databaseSchema != null)
    //    {
    //        await _databaseSchema.DisposeAsync();
    //    }

    //    await base.DisposeAllAsync();
    //}

    #region 过滤

    /// <summary>
    /// 添加检索过滤
    /// </summary>
    /// <param name="filter"></param>
    /// <returns></returns>
    public virtual IQueryRepository<T> AddQueryFilter(Expression<Func<T, bool>>? filter = null)
    {
        Filter = filter;
        return this;
    }

    /// <summary>
    /// 忽略查询过滤条件
    /// </summary>
    /// <returns></returns>
    public virtual IQueryRepository<T> IgnoreQueryFilter()
    {
        IsIgnoreQueryFilter = true;
        return this;
    }

    /// <summary>
    /// 恢复忽略查询过滤条件
    /// </summary>
    /// <returns></returns>
    public virtual IQueryRepository<T> RecoveryQueryFilter()
    {
        IsIgnoreQueryFilter = false;
        return this;
    }

    #endregion

    #region 查询 复杂型

    /// <summary>
    /// 查询
    /// </summary>
    /// <param name="isTracking">是否追踪</param>
    /// <returns></returns>
    [Obsolete("请使用 GetAll 函数代替.")]
    public virtual IQueryable<T> Query(bool isTracking = true) => GetAll(isTracking);

    /// <summary>
    /// 查询 有跟踪
    /// </summary>
    public virtual IQueryable<T> Select => GetAll();

    /// <summary>
    /// 查询 无跟踪
    /// </summary>
    public virtual IQueryable<T> SelectNoTracking => GetAll(false);

    /// <summary>
    /// 查询
    /// </summary>
    /// <param name="isTracking"></param>
    /// <returns></returns>
    public virtual IQueryable<T> GetAll(bool isTracking = true)
    {
        return isTracking
            ? UnitOfWork.DbSet<T>().WhereIf(!IsIgnoreQueryFilter && Filter != null, Filter!).AsQueryable()
            : UnitOfWork.DbSet<T>().WhereIf(!IsIgnoreQueryFilter && Filter != null, Filter!).AsNoTracking();
    }

    #endregion

    #region 查询 单条

    /// <summary>
    /// 查询 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual T? Find(Expression<Func<T, bool>> expWhere)
        => Select.Where(expWhere).FirstOrDefault();

    /// <summary>
    /// 查询 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual T? Get(Expression<Func<T, bool>> expWhere)
        => Find(expWhere);

    /// <summary>
    /// 查询 根据id
    /// </summary>
    /// <param name="key"></param>
    /// <typeparam name="TKey"></typeparam>
    /// <returns></returns>
    public virtual T? FindById<TKey>(TKey key)
        => Select.FirstOrDefault(GetKeyExpression(key));

    /// <summary>
    /// 查询 根据id
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="key"></param>
    /// <returns></returns>
    public virtual T? GetById<TKey>(TKey key)
        => FindById(key);

    /// <summary>
    /// 查询 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<T?> FindAsync(Expression<Func<T, bool>> expWhere)
        => Select.Where(expWhere).FirstOrDefaultAsync();

    /// <summary>
    /// 查询 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<T?> GetAsync(Expression<Func<T, bool>> expWhere)
        => FindAsync(expWhere);

    /// <summary>
    /// 查询 根据id
    /// </summary>
    /// <param name="key"></param>
    /// <typeparam name="TKey"></typeparam>
    /// <returns></returns>
    public virtual Task<T?> FindByIdAsync<TKey>(TKey key)
        => Select.FirstOrDefaultAsync(GetKeyExpression(key));

    /// <summary>
    /// 查询 根据id
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="key"></param>
    /// <returns></returns>
    public virtual Task<T?> GetByIdAsync<TKey>(TKey key)
        => FindByIdAsync(key);

    #endregion

    #region 查询 多条

    /// <summary>
    /// 获取列表 根据查询条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual List<T> ToList(Expression<Func<T, bool>> expWhere)
        => Select.Where(expWhere).ToList();

    /// <summary>
    /// 获取列表 根据查询条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual List<T> GetAllList(Expression<Func<T, bool>> expWhere)
        => ToList(expWhere);

    /// <summary>
    /// 获取所有数据
    /// </summary>
    /// <returns></returns>
    public virtual List<T> ToListAll() => Select.ToList();

    /// <summary>
    /// 获取所有数据
    /// </summary>
    /// <returns></returns>
    public virtual List<T> GetAllList()
        => ToListAll();

    /// <summary>
    /// 获取列表 根据查询条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<List<T>> ToListAsync(Expression<Func<T, bool>> expWhere)
        => Select.Where(expWhere).ToListAsync();

    /// <summary>
    /// 获取列表 根据查询条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<List<T>> GetAllListAsync(Expression<Func<T, bool>> expWhere)
        => ToListAsync(expWhere);

    /// <summary>
    /// 获取所有数据
    /// </summary>
    /// <returns></returns>
    public virtual Task<List<T>> ToListAllAsync()
        => Select.ToListAsync();

    /// <summary>
    /// 获取所有数据
    /// </summary>
    /// <returns></returns>
    public virtual Task<List<T>> GetAllListAsync()
        => ToListAllAsync();

    #endregion

    #region 是否存在 、 数量

    /// <summary>
    /// 获取数量
    /// </summary>
    /// <returns></returns>
    public virtual int Count()
        => Select.Count();

    /// <summary>
    /// 获取数量
    /// </summary>
    /// <returns></returns>
    public virtual long CountLong()
        => Select.LongCount();

    /// <summary>
    /// 获取数量 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual int Count(Expression<Func<T, bool>> expWhere)
        => Select.Count(expWhere);

    /// <summary>
    /// 获取数量 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual long CountLong(Expression<Func<T, bool>> expWhere)
        => Select.LongCount(expWhere);

    /// <summary>
    /// 是否存在 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual bool Any(Expression<Func<T, bool>> expWhere)
        => Select.Any(expWhere);

    /// <summary>
    /// 获取数量
    /// </summary>
    /// <returns></returns>
    public virtual Task<int> CountAsync()
        => Select.CountAsync();

    /// <summary>
    /// 获取数量
    /// </summary>
    /// <returns></returns>
    public virtual Task<long> CountLongAsync()
        => Select.LongCountAsync();

    /// <summary>
    /// 获取数量 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<int> CountAsync(Expression<Func<T, bool>> expWhere)
        => Select.CountAsync(expWhere);

    /// <summary>
    /// 获取数量 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<long> CountLongAsync(Expression<Func<T, bool>> expWhere)
        => Select.LongCountAsync(expWhere);

    /// <summary>
    /// 是否存在 根据条件
    /// </summary>
    /// <param name="expWhere"></param>
    /// <returns></returns>
    public virtual Task<bool> AnyAsync(Expression<Func<T, bool>> expWhere)
        => Select.AnyAsync(expWhere);

    #endregion

    #region 原生 sql 操作

    /// <summary>
    /// 查询根据sql语句
    /// EFCore 原生sql查询
    /// </summary>
    /// <returns> IQueryable </returns>
    public virtual IQueryable<T> QueryableBySql(string sql, params object[] parameters)
    {
        return UnitOfWork.DbSet<T>().FromSqlRaw(sql, parameters);
    }

    /// <summary>
    /// 根据 sql 查询表格
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual DataTable? QueryDataTableBySql(string sql, params object[] parameters)
    {
        return Context.Database.QueryDataTableBySql(sql, parameters);
    }

    /// <summary>
    /// 根据 sql 查询表格
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual Task<DataTable?> QueryDataTableBySqlAsync(string sql, params object[] parameters)
    {
        return Context.Database.QueryDataTableBySqlAsync(sql, parameters);
    }

    /// <summary>
    /// 根据 sql 查询字典集合
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual List<Dictionary<string, object?>>? QueryDicBySql(string sql, params object[] parameters)
    {
        return Context.Database.QueryDicBySql(sql, parameters);
    }

    /// <summary>
    /// 根据 sql 查询字典集合
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual Task<List<Dictionary<string, object?>>?> QueryDicBySqlAsync(string sql, params object[] parameters)
    {
        return Context.Database.QueryDicBySqlAsync(sql, parameters);
    }

    /// <summary>
    /// 查询根据sql语句
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual List<T>? QueryBySql(string sql, params object[] parameters)
    {
        return Context.Database.QueryBySql<T>(sql, parameters);
    }

    /// <summary>
    /// 查询根据sql语句
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual Task<List<T>?> QueryBySqlAsync(string sql, params object[] parameters)
    {
        return Context.Database.QueryBySqlAsync<T>(sql, parameters);
    }

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual object? QuerySingleBySql(string sql, params object[] parameters)
    {
        return Context.Database.QuerySingleBySql(sql, parameters);
    }

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual Task<object?> QuerySingleBySqlAsync(string sql, params object[] parameters)
    {
        return Context.Database.QuerySingleBySqlAsync(sql, parameters);
    }

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual TResult QuerySingleBySql<TResult>(string sql, params object[] parameters)
        where TResult : struct
    {
        return Context.Database.QuerySingleBySql<TResult>(sql, parameters);
    }

    /// <summary>
    /// 查询根据sql返回单个值
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public virtual Task<TResult> QuerySingleBySqlAsync<TResult>(string sql, params object[] parameters)
        where TResult : struct
    {
        return Context.Database.QuerySingleBySqlAsync<TResult>(sql, parameters);
    }

    #endregion

    #region 查询数据库结构

    /// <summary>
    /// 获取所有的表
    /// </summary>
    /// <returns></returns>
    public List<TableModel> GetTables()
    {
        return _databaseSchema?.GetTables() ?? new List<TableModel>();
    }

    /// <summary>
    /// 获取所有的列
    /// </summary>
    /// <returns></returns>
    public List<ColumnModel> GetColumns()
    {
        return _databaseSchema?.GetColumns() ?? new List<ColumnModel>();
    }

    /// <summary>
    /// 获取所有的数据类型
    /// </summary>
    /// <returns></returns>
    public List<DataTypeModel> GetDataTypes()
    {
        return _databaseSchema?.GetDataTypes() ?? new List<DataTypeModel>();
    }

    #endregion
}