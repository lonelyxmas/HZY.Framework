﻿/*
 * *******************************************************
 *
 * 作者：hzy
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */

using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace HZY.Framework.Repository.EntityFramework.Repositories.Impl;

/// <summary>
/// 默认仓储实现类
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="TDbContext"></typeparam>
public class RepositoryImpl<T, TDbContext> : RepositoryBaseImpl<T, TDbContext>, IRepository<T>
    where T : class, new()
    where TDbContext : DbContext
{
    /// <summary>
    /// 默认仓储实现类
    /// </summary>
    /// <param name="dbContext"></param>
    /// <param name="filter"></param>
    public RepositoryImpl(TDbContext dbContext, Expression<Func<T, bool>>? filter = null) : base(dbContext, filter)
    {
    }
}