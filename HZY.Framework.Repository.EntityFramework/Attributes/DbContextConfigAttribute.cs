﻿namespace HZY.Framework.Repository.EntityFramework.Attributes;

/// <summary>
/// DbContext 配置注解
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class DbContextConfigAttribute : Attribute
{
    /// <summary>
    /// 命名规则默认 蛇形 命名 SysFunction => sys_function
    /// </summary>
    public NameRuleType NameRuleType { get; set; } = NameRuleType.SnakeCase;

    /// <summary>
    /// 实体命名空间
    /// </summary>
    public string EntityNamespace { get; set; }

    /// <summary>
    /// DbContext 配置注解
    /// </summary>
    /// <param name="entityNamespace"></param>
    public DbContextConfigAttribute(string entityNamespace)
    {
        EntityNamespace = entityNamespace;
    }

    /// <summary>
    /// DbContext 配置注解
    /// </summary>
    /// <param name="entityNamespace"></param>
    /// <param name="nameRuleType"></param>
    public DbContextConfigAttribute(string entityNamespace, NameRuleType nameRuleType)
    {
        EntityNamespace = entityNamespace;
        NameRuleType = nameRuleType;
    }

    /// <summary>
    /// 获取模型类型集合
    /// </summary>
    /// <param name="dbContextType"></param>
    /// <param name="predicate"></param>
    /// <returns></returns>
    public List<Type> GetModelTypes(Type dbContextType, Func<Type, bool>? predicate = null)
    {
        var assembly = dbContextType.Assembly;
        var types = (from w in assembly.ExportedTypes.ToList()
                     where w.IsClass && w.IsPublic && !w.IsGenericType && !w.IsAbstract
                     where w.GetInterface(nameof(IEntity)) != null || typeof(IEntity).IsAssignableFrom(w)
                     //where !w.Name.StartsWith(nameof(DefaultEntity)) && !w.Name.Contains(nameof(FullEntity))
                     select w)
                .WhereIf(predicate != null, predicate!)
                .Where(w => Regex.IsMatch(w.FullName!, EntityNamespace))
                .ToList()
            ;

        //扫描类型下面的 dbset model
        var propertyInfoList =
            dbContextType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        var dbSets = propertyInfoList.Where(w => w.PropertyType.Name == "DbSet`1");

        foreach (var dbSet in dbSets)
        {
            if (dbSet.PropertyType.GenericTypeArguments.Length <= 0) continue;

            var model = dbSet.PropertyType.GenericTypeArguments[0];

            if (types.Any(w => w.FullName == model.FullName)) continue;

            types.Add(model);
        }

        return types;
    }

    /// <summary>
    /// 模型创建
    /// </summary>
    /// <param name="modelBuilder"></param>
    /// <param name="modelTypes"></param>
    /// <returns></returns>
    public List<Type> OnModelCreating(ModelBuilder modelBuilder, List<Type> modelTypes)
    {
        #region 自动注册 dbset

        var types = modelTypes;

        foreach (var type in types)
        {
            if (modelBuilder.Model.FindEntityType(type) != null)
            {
                continue;
            }

            modelBuilder.Model.AddEntityType(type);
        }

        #endregion

        // 开始命中删除字段
        var deleteEntityAllList = modelBuilder.Model
               .GetEntityTypes()
               .Where(w => typeof(IEntity).IsAssignableFrom(w.ClrType))
           ;
        foreach (var item in deleteEntityAllList)
        {
            // 循环审计字段 集合
            var deleteFieldNameList = RepositoryEntityFrameworkExtensions.AuditOptions
                .Where(w => !string.IsNullOrWhiteSpace(w.IsDeletedFieldName))
                .Select(w => w.IsDeletedFieldName)
                .Distinct()
                ;
            foreach (var delName in deleteFieldNameList)
            {
                var propertyInfo = item.ClrType.GetProperty(delName!);
                if (propertyInfo is null) continue;

                var propertyType = propertyInfo.PropertyType;

                if (propertyType == typeof(bool) || propertyType == typeof(bool?))
                {
                    var lambda = ExpressionTreeExtensions.NotEqual(item.ClrType, delName!, true, propertyType);
                    modelBuilder.Entity(item.ClrType).HasQueryFilter(lambda);
                }
            }
        }

        #region 过滤软删除 条件是：实体必须继承自 IBaseDeleteEntity

        var deleteEntityList = modelBuilder.Model
                .GetEntityTypes()
                .Where(w => typeof(IBaseDeleteEntity).IsAssignableFrom(w.ClrType))
            ;
        foreach (var item in deleteEntityList)
        {
            var lambda = ExpressionTreeExtensions.NotEqual(item.ClrType, nameof(IBaseDeleteEntity.IsDeleted), true, typeof(bool));
            modelBuilder.Entity(item.ClrType).HasQueryFilter(lambda);
        }

        #endregion

        #region 过滤软删除 条件是：实体必须继承自 IBaseDeleteEntityV2

        var deleteEntityV2List = modelBuilder.Model
                .GetEntityTypes()
                .Where(w => typeof(IBaseDeleteEntityV2).IsAssignableFrom(w.ClrType))
            ;
        foreach (var item in deleteEntityV2List)
        {
            var lambda = ExpressionTreeExtensions.NotEqual(item.ClrType, nameof(IBaseDeleteEntityV2.DeleteFlag), true, typeof(bool?));
            modelBuilder.Entity(item.ClrType).HasQueryFilter(lambda);
        }

        #endregion

        #region 自动映射表名

        NamingConvention(modelBuilder, NameRuleType);

        #endregion

        return types;
    }

    /// <summary>
    /// 命名规则转化
    /// </summary>
    protected static void NamingConvention(ModelBuilder modelBuilder, NameRuleType nameRuleType)
    {
        foreach (var entity in modelBuilder.Model.GetEntityTypes())
        {
            NamingConventionToTableName(entity, nameRuleType);
            NamingConventionToFieldName(entity, nameRuleType);
        }
    }

    /// <summary>
    /// 表名称命名规则
    /// </summary>
    /// <param name="mutableEntityType"></param>
    /// <param name="nameRuleType"></param>
    protected static void NamingConventionToTableName(IMutableEntityType mutableEntityType, NameRuleType nameRuleType)
    {
        var type = mutableEntityType.ClrType;

        var tableAttribute = type.GetCustomAttribute<TableAttribute>();
        var entityDescriptionAttribute = type.GetCustomAttribute<EntityDescriptionAttribute>();

        if (tableAttribute != null) return;

        var oldName = mutableEntityType.GetTableName() ?? type.Name;
        var _nameRuleType = entityDescriptionAttribute?.NameRuleType ?? nameRuleType;
        mutableEntityType.SetTableName(GetNameByNameRuleType(oldName, _nameRuleType));

        //mutableEntityType.SetTableName(tableAttribute.Name);
    }

    /// <summary>
    /// 字段名称命名规则
    /// </summary>
    /// <param name="mutableEntityType"></param>
    /// <param name="nameRuleType"></param>
    protected static void NamingConventionToFieldName(IMutableEntityType mutableEntityType, NameRuleType nameRuleType)
    {
        var type = mutableEntityType.ClrType;

        var entityDescriptionAttribute = type.GetCustomAttribute<EntityDescriptionAttribute>();

        if (entityDescriptionAttribute != null && entityDescriptionAttribute.FieldIgnored) return;

        var mutableProperties = mutableEntityType.GetProperties();

        foreach (var item in mutableProperties)
        {
            if (item.PropertyInfo is null)
            {
                continue;
            }

            var columnAttribute = item.PropertyInfo.GetCustomAttribute<ColumnAttribute>();
            if (columnAttribute is not null && !string.IsNullOrWhiteSpace(columnAttribute.Name))
            {
                continue;
            }

            var _nameRuleType = entityDescriptionAttribute == null
                ? nameRuleType
                : entityDescriptionAttribute.NameRuleType;
            var oldColumnName = item.GetColumnName();
            var newColumnName = GetNameByNameRuleType(oldColumnName, nameRuleType);
            item.SetColumnName(newColumnName);
        }
    }

    /// <summary>
    /// 获取名称根据命名规则
    /// </summary>
    /// <param name="oldName"></param>
    /// <param name="nameRuleType"></param>
    /// <returns></returns>
    private static string GetNameByNameRuleType(string oldName, NameRuleType nameRuleType)
    {
        string newName = string.Empty;
        switch (nameRuleType)
        {
            case NameRuleType.Default:
                break;
            case NameRuleType.SnakeCase:
                // 蛇形命名法
                // ToUnderlineNomenclature()  将驼峰命名法改为蛇形命名法  类似: SysFunction => sys_function
                newName = ToUnderlineNomenclature(oldName);
                break;
            case NameRuleType.UpperSnakeCase:
                // 全大写蛇形命名法
                //  SysFunction => SYS_FUNCTION
                newName = ToUnderlineNomenclature(oldName).ToUpper();
                break;
            case NameRuleType.Upper:
                // 表名全大写
                newName = oldName.ToUpper();
                break;
            case NameRuleType.Lower:
                // 表名全小写
                newName = oldName.ToLower();
                break;
        }

        return newName;
    }

    /// <summary>将驼峰命名法改为 下划线 命名法</summary>
    /// <param name="input"></param>
    /// <returns></returns>
    static string ToUnderlineNomenclature(string input) => string.IsNullOrWhiteSpace(input)
        ? input
        : Regex.Replace(input, "([a-z0-9])([A-Z])", "$1_$2").ToLower();

    // 判断是否为可空类型
    static bool IsNullableType(Type type) => type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);

}