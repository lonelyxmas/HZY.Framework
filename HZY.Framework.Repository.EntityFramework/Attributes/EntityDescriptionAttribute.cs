﻿namespace HZY.Framework.Repository.EntityFramework.Attributes;

/// <summary>
/// 实体描述
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class EntityDescriptionAttribute : Attribute
{
    /// <summary>
    /// 命名规则默认 蛇形命名 SysFunction => sys_function
    /// 
    /// <p>如果有 [Table("")] 以他为表名，否则安装规则进行命名</p>
    /// 
    /// </summary>
    public NameRuleType NameRuleType { get; set; } = NameRuleType.SnakeCase;

    /// <summary>
    /// 忽略字段采用命名规则
    /// <para>
    /// true    = 只有表名满足命名规则条件
    /// false   = 表名称和字段名称都使用命名规则
    /// </para>
    /// </summary>
    public bool FieldIgnored { get; set; } = false;

    /// <summary>
    /// 实体描述
    /// </summary>
    public EntityDescriptionAttribute()
    {

    }

    /// <summary>
    /// 实体描述
    /// </summary>
    /// <param name="nameRuleType"></param>
    public EntityDescriptionAttribute(NameRuleType nameRuleType)
    {
        NameRuleType = nameRuleType;
    }



}
