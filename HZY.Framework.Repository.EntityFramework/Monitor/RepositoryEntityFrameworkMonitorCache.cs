﻿namespace HZY.Framework.Repository.EntityFramework.Monitor;

/// <summary>
/// 监控EFCore缓存
/// </summary>
public static class RepositoryEntityFrameworkMonitorCache
{
    private static readonly EntityFrameworkRepositoriesMonitorContext? EfCoreMonitorContext;
    private static readonly List<EntityFrameworkRepositoriesMonitorSqlContext>? EfCoreMonitorSqlContextList;

    static RepositoryEntityFrameworkMonitorCache()
    {
        EfCoreMonitorContext ??= new EntityFrameworkRepositoriesMonitorContext();
        EfCoreMonitorSqlContextList ??= new List<EntityFrameworkRepositoriesMonitorSqlContext>();
    }

    /// <summary>
    /// efCore 监控上下文
    /// </summary>
    public static EntityFrameworkRepositoriesMonitorContext? Context => EfCoreMonitorContext;

    /// <summary>
    /// sql 监控上下文
    /// </summary>
    public static List<EntityFrameworkRepositoriesMonitorSqlContext>? SqlContext => EfCoreMonitorSqlContextList;

    #region IDbConnectionInterceptor

    /// <summary>
    /// 设置打开连接数量
    /// </summary>
    public static void OpenDbConnectionCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.OpenDbConnectionCount++;
    }

    /// <summary>
    /// 设置关闭连接数量
    /// </summary>
    public static void CloseDbConnectionCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.CloseDbConnectionCount++;
    }

    /// <summary>
    /// 设置连接失败数量
    /// </summary>
    public static void ConnectionFailedCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.ConnectionFailedCount++;
    }

    #endregion

    #region IDbCommandInterceptor

    /// <summary>
    /// 创建命令数量
    /// </summary>
    public static void CreateCommandCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.CreateCommandCount++;
    }

    /// <summary>
    /// 执行命令数量
    /// </summary>
    public static void ExecuteCommandCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.ExecuteCommandCount++;
    }

    /// <summary>
    /// 命令执行失败数量
    /// </summary>
    public static void CommandFailedCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.ExecuteCommandCount++;
    }

    #endregion

    #region IDbTransactionInterceptor

    /// <summary>
    /// 创建事务 数量
    /// </summary>
    public static void CreateTransactionCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.CreateTransactionCount++;
    }

    /// <summary>
    /// 提交事务 数量
    /// </summary>
    public static void SubmitTransactionCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.SubmitTransactionCount++;
    }

    /// <summary>
    /// 回滚事务 数量
    /// </summary>
    public static void RollBackCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.RollBackCount++;
    }

    /// <summary>
    /// 事务失败 数量
    /// </summary>
    public static void TransactionFailedCount()
    {
        if (EfCoreMonitorContext == null) return;
        EfCoreMonitorContext.TransactionFailedCount++;
    }

    #endregion

    /// <summary>
    /// 设置 sql 信息
    /// </summary>
    /// <param name="dbCommand"></param>
    /// <param name="elapsedMilliseconds"></param>
    public static void SetSqlInfo(DbCommand dbCommand, long elapsedMilliseconds)
    {
        if (EfCoreMonitorSqlContextList is null) return;

        if (EfCoreMonitorSqlContextList.Count > 100000)
        {
            EfCoreMonitorSqlContextList.Clear();
        }

        EfCoreMonitorSqlContextList.Add(new EntityFrameworkRepositoriesMonitorSqlContext
        {
            Sql = dbCommand.CommandText,
            ElapsedMilliseconds = elapsedMilliseconds
        });
    }
}