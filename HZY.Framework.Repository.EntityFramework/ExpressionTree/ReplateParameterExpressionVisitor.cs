namespace HZY.Framework.Repository.EntityFramework.ExpressionTree;

/// <summary>
/// 将拉姆达参数 w=> 部分修改替换
/// </summary>
/// <typeparam name="T"></typeparam>
public class ReplaceParameterExpressionVisitor : ExpressionVisitor
{
    /// <summary>
    /// 被替换后新的参数名称
    /// </summary>
    private readonly string _parameterName;
    private readonly Type _type;

    /// <summary>
    /// ReplaceParameterExpressionVisitor
    /// </summary>
    /// <param name="parameterName"></param>
    /// <param name="type"></param>
    public ReplaceParameterExpressionVisitor(string parameterName, Type type)
    {
        _parameterName = parameterName;
        _type = type;
    }

    /// <summary>
    /// Visit
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    [return: NotNullIfNotNull("node")]
    public override Expression? Visit(Expression? node)
    {
        return base.Visit(node);
    }

    /// <summary>
    /// VisitParameter
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    protected override Expression VisitParameter(ParameterExpression node)
    {
        return Expression.Parameter(_type, _parameterName);
    }
}