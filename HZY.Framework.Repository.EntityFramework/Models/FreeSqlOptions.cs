﻿namespace HZY.Framework.Repository.EntityFramework.Models;

/// <summary>
/// freesql 配置
/// </summary>
public class FreeSqlOptions
{

    /// <summary>
    /// freesql 构建 配置
    /// </summary>
    public Action<FreeSqlBuilder>? FreeSqlBuilderAction { get; set; }

    /// <summary>
    /// freesql 配置
    /// </summary>
    public Action<IFreeSql>? FreeSqlAction { get; set; }

    /// <summary>
    /// 审计拦截器
    /// </summary>
    public List<AbstractFreeSqlAuditAop> FreeSqlAuditAopList { get; set; } = new();

}
