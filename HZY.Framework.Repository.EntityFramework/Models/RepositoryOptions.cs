﻿namespace HZY.Framework.Repository.EntityFramework.Models;

/// <summary>
/// 仓储配置
/// </summary>
public class RepositoryOptions
{

    /// <summary>
    /// 数据库类型
    /// </summary>
    public DefaultDatabaseType DefaultDatabaseType { get; set; } = DefaultDatabaseType.SqlServer;

    /// <summary>
    /// 是否监控 efcore
    /// </summary>
    public bool IsMonitorEFCore { get; set; } = false;

    /// <summary>
    /// 数据库连接字符串
    /// </summary>
    public string ConnectionString { get; set; } = string.Empty;

    /// <summary>
    /// 主库 数据库连接字符串
    /// </summary>
    //public string? MasterConnectionString { get; set; }

    /// <summary>
    /// 从库 数据库连接字符串
    /// </summary>
    public string[]? SlaveConnectionStrings { get; set; }

}
