﻿namespace HZY.Framework.Repository.EntityFramework.Models.Enums;

/// <summary>
/// 默认数据库类型
/// </summary>
public enum DefaultDatabaseType
{

    SqlServer,
    MySql,
    PostgreSql,
    Oracle

}
