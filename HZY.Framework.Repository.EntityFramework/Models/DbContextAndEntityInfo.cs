﻿namespace HZY.Framework.Repository.EntityFramework.Models;

/// <summary>
/// 
/// </summary>
public class DbContextAndEntityInfo
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="entityType"></param>
    /// <param name="dbContextType"></param>
    public DbContextAndEntityInfo(Type entityType, Type dbContextType)
    {
        EntityType = entityType;
        DbContextType = dbContextType;
    }

    /// <summary>
    /// 
    /// </summary>
    public Type EntityType { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Type DbContextType { get; set; }

}
