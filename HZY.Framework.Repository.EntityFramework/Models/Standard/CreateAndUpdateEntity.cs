﻿namespace HZY.Framework.Repository.EntityFramework.Models.Standard;

/// <summary>
/// 创建和更新
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class CreateAndUpdateEntity<TKey> : Entity<TKey>, ICreateAndUpdateEntity
{
    /// <summary>
    /// 创建人 id
    /// </summary>
    public virtual Guid? CreatorUserId { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public virtual DateTime CreationTime { get; set; } = DateTime.Now;

    /// <summary>
    /// 最后更新人
    /// </summary>
    public virtual Guid? LastModifierUserId { get; set; }

    /// <summary>
    /// 最后更新时间
    /// </summary>
    public virtual DateTime? LastModificationTime { get; set; }
}

/// <summary>
/// 创建
/// </summary>
public interface ICreateAndUpdateEntity : IEntity
{
    /// <summary>
    /// 创建用户
    /// </summary>
    Guid? CreatorUserId { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime CreationTime { get; set; }
    /// <summary>
    /// 更新用户
    /// </summary>
    Guid? LastModifierUserId { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    DateTime? LastModificationTime { get; set; }
}