﻿namespace HZY.Framework.Repository.EntityFramework.Models.Standard;

/// <summary>
/// 删除
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class DeleteEntity<TKey> : CreateAndUpdateEntity<TKey>, IDeleteEntity
{
    /// <summary>
    /// 软删除标识
    /// </summary>
    public virtual bool IsDeleted { get; set; } = false;

    /// <summary>
    /// 删除人 id
    /// </summary>
    public virtual Guid? DeleterUserId { get; set; }

    /// <summary>
    /// 删除时间
    /// </summary>
    public virtual DateTime? DeletionTime { get; set; }
}

/// <summary>
/// 删除
/// </summary>
public interface IDeleteEntity : IBaseDeleteEntity
{
    /// <summary>
    /// 删除用户
    /// </summary>
    Guid? DeleterUserId { get; set; }

    /// <summary>
    /// 删除时间
    /// </summary>
    DateTime? DeletionTime { get; set; }
}

/// <summary>
/// 删除
/// </summary>
public interface IBaseDeleteEntity : IEntity
{
    /// <summary>
    /// 是否删除
    /// </summary>
    bool IsDeleted { get; set; }
}