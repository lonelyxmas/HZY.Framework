﻿namespace HZY.Framework.Repository.EntityFramework.Models.StandardV2;

/// <summary>
/// 删除
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class DeleteEntityV2<TKey> : CreateAndUpdateEntityV2<TKey>, IDeleteEntityV2
{
    /// <summary>
    /// 软删除标识
    /// </summary>
    public virtual bool? DeleteFlag { get; set; } = false;

    /// <summary>
    /// 删除人 id
    /// </summary>
    public virtual string? DeleteBy { get; set; }

    /// <summary>
    /// 删除时间
    /// </summary>
    public virtual DateTime? DeleteTime { get; set; }
}

/// <summary>
/// 删除
/// </summary>
public interface IDeleteEntityV2 : IBaseDeleteEntityV2
{
    /// <summary>
    /// 删除用户
    /// </summary>
    string? DeleteBy { get; set; }

    /// <summary>
    /// 删除时间
    /// </summary>
    DateTime? DeleteTime { get; set; }
}

/// <summary>
/// 删除
/// </summary>
public interface IBaseDeleteEntityV2 : IEntity
{
    /// <summary>
    /// 是否删除
    /// </summary>
    bool? DeleteFlag { get; set; }
}