﻿namespace HZY.Framework.Repository.EntityFramework.Models.StandardV2;

/// <summary>
/// 创建和更新
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class CreateAndUpdateEntityV2<TKey> : Entity<TKey>, ICreateAndUpdateEntityV2
{
    /// <summary>
    /// 创建人 id
    /// </summary>
    public virtual string? CreateBy { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public virtual DateTime CreateTime { get; set; } = DateTime.Now;

    /// <summary>
    /// 最后更新人id
    /// </summary>
    public virtual string? UpdateBy { get; set; }

    /// <summary>
    /// 最后更新时间
    /// </summary>
    public virtual DateTime? UpdateTime { get; set; }
}

/// <summary>
/// 创建
/// </summary>
public interface ICreateAndUpdateEntityV2 : IEntity
{
    /// <summary>
    /// 创建用户
    /// </summary>
    string? CreateBy { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime CreateTime { get; set; }

    /// <summary>
    /// 更新用户
    /// </summary>
    string? UpdateBy { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    DateTime? UpdateTime { get; set; }
}