﻿namespace HZY.Framework.Repository.EntityFramework;

/// <summary>
/// 默认仓储
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IRepository<T> : IRepositoryBase<T> where T : class, new()
{



}