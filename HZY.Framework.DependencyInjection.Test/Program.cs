using HZY.Framework.Aop.Attributes;
using HZY.Framework.DependencyInjection;
using HZY.Framework.DependencyInjection.Attributes;
using HZY.Framework.DependencyInjection.Test;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDependencyInjection(new List<Assembly> { typeof(User).Assembly });

builder.Services.AddTransient<IReposiroty<User>, Reposiroty<User>>();
builder.Services.AddTransient<UserService>();

builder.Host.ReplaceServiceProvider();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// 设置host
AopMoAttribute.SetHost(app);

// 设置服务提供者
app.Use(async (context, next) =>
{
    AopMoAttribute.SetServiceProvider(context.RequestServices);
    await next?.Invoke(context)!;
});


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
