using HZY.Framework.DependencyInjection.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace HZY.Framework.DependencyInjection.Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        private readonly UserService _userService;
        private readonly IReposiroty<User> _user;

        [AppSettings("AllowedHosts")]
        protected string AllowedHosts { get; set; }


        public WeatherForecastController(ILogger<WeatherForecastController> logger, UserService userService, IReposiroty<User> user)
        {
            //var a = serviceProvider.GetService<UserService>();
            _logger = logger;
            _userService = userService;
            _user = user;
        }

        [HttpGet]
        public string GetUser()
        {
            return _userService.GetUserName();
        }


    }
}
