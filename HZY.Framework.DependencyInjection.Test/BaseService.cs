﻿using HZY.Framework.Aop;
using HZY.Framework.DependencyInjection.Attributes;
using HZY.Framework.DependencyInjection.Services;

namespace HZY.Framework.DependencyInjection.Test;

public class BaseService<T> : IAopServiceProvider
{
    [Autowired]
    protected IReposiroty<T>? Reposiroty { get; }

    public IServiceProvider ServiceProvider { get; set; }

    [Autowired]
    public BaseService(IServiceProvider serviceProvider)
    {

    }

}



//public class BaseService<T> : IAopServiceProvider
//{
//    [Autowired]
//    private IReposiroty<T> Reposiroty { get; }

//    public IServiceProvider ServiceProvider { get; set; }

//    public BaseService(IServiceProvider serviceProvider)
//    {
//        ServiceProvider = serviceProvider;
//    }

//}

//public class BaseService<T>
//{
//    [Autowired]
//    private IReposiroty<T> Reposiroty { get; }

//    private readonly IServiceProvider _serviceProvider;

//    public BaseService(IServiceProvider serviceProvider)
//    {
//        _serviceProvider = serviceProvider;
//    }

//}

//public class BaseService<T>
//{
//    [Autowired]
//    private IReposiroty<T> Reposiroty { get; }

//    [Autowired]
//    public BaseService(IServiceProvider serviceProvider)
//    {
//        var a = Reposiroty;
//    }

//}

//public class BaseService<T> : IAopServiceProvider
//{
//    [Autowired]
//    private IReposiroty<T> Reposiroty { get; }

//    public IServiceProvider ServiceProvider { get; set; }

//    public BaseService(IServiceProvider serviceProvider)
//    {
//        ServiceProvider = serviceProvider;
//        var a = Reposiroty;
//    }

//}
