﻿using HZY.Framework.Core.AspNetCore;
using HZY.Framework.Core.ServerMetricMonitoring;
using HZY.Framework.DependencyInjection;
using Newtonsoft.Json;

namespace HZY.Framework.Core.Test
{
    [ImportStartupModule(typeof(TestApp1Startup))]
    public class TestAppStartup : StartupModule<TestAppStartup>
    {
        public TestAppStartup()
        {
            this.Order = 2;
        }

        public override void ConfigureServices(WebApplicationBuilder webApplicationBuilder)
        {
            // Add services to the container.

            webApplicationBuilder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            webApplicationBuilder.Services.AddEndpointsApiExplorer();
            webApplicationBuilder.Services.AddSwaggerGen();

            webApplicationBuilder.Services.AddTransient<JobApp>();

            // 扫描服务自动化注册
            webApplicationBuilder.Services
                .AddDependencyInjection(App.Startups.Select(w => w.GetType().Assembly).ToList());
        }

        public override void Configure(WebApplication webApplication)
        {
            // Configure the HTTP request pipeline.
            if (webApplication.Environment.IsDevelopment())
            {
                webApplication.UseSwagger();
                webApplication.UseSwaggerUI();
            }

            webApplication.UseHttpsRedirection();

            webApplication.UseAuthorization();

            webApplication.MapControllers();

            try
            {
                webApplication.Services.CreateScope().ServiceProvider.GetService<JobApp>()?.Run();
            }
            catch (Exception)
            {
            }

            var serverMetricMonitoringService = webApplication.Services.GetRequiredService<IServerMetricMonitoringService>();

            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(1000);

                    var a1 = serverMetricMonitoringService.GetComputerInfo();
                    Console.WriteLine(JsonConvert.SerializeObject(a1));

                    var a2 = serverMetricMonitoringService.GetDiskInfos();
                    Console.WriteLine(JsonConvert.SerializeObject(a2));

                    var a3 = serverMetricMonitoringService.GetCpuRate();
                    Console.WriteLine(JsonConvert.SerializeObject(a3));

                    var a4 = serverMetricMonitoringService.GetRunTime();
                    Console.WriteLine(JsonConvert.SerializeObject(a4));
                }
            });





        }

    }
}
