﻿using HZY.Framework.Core.AspNetCore;

namespace HZY.Framework.Core.Test;

public class TestApp2Startup : StartupModule<TestApp2Startup>
{

    public override void ConfigureServices(WebApplicationBuilder webApplicationBuilder)
    {
        Console.WriteLine($"{nameof(TestApp2Startup)} = {nameof(ConfigureServices)}");
    }

    public override void Configure(WebApplication webApplication)
    {
        Console.WriteLine($"{nameof(TestApp2Startup)} = {nameof(Configure)}");
    }


}
