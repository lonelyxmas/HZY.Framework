using HZY.Framework.Core;
using HZY.Framework.Core.Test;
using HZY.Framework.Core.Utils;


var v = ObjectUtil.ToInt32("123.11"); // 123
var v1 = ObjectUtil.ToInt32("123"); // 123
var v2 = ObjectUtil.ToInt32(222.8); // 123
var v3 = ObjectUtil.ToInt32(DateTime.Now); // 123
var v4 = ObjectUtil.ToInt32(11); // 123

HzyApplication.Run<TestAppStartup>(args);