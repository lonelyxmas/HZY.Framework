﻿/*
 * *******************************************************
 *
 * 作者：hzy
 *
 * 开源地址：https://gitee.com/hzy6
 *
 * *******************************************************
 */
namespace HZY.Framework.DependencyInjection;

public static class DependencyInjectionStartupConfig
{
    /// <summary>
    /// 自动注册服务
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="assemblyList"></param>
    public static void AddDependencyInjection(this IServiceCollection serviceCollection, List<Assembly>? assemblyList)
    {
        if (assemblyList == null) return;
        DynamicProxyClass.ScanningDiInterface(serviceCollection, assemblyList);
    }

    /// <summary>
    /// 自动注册服务 通过 ComponentAttribute 特性
    /// </summary>
    /// <param name="serviceCollection"></param>
    /// <param name="assemblyList"></param>
    public static void AddDependencyInjectionByComponent(this IServiceCollection serviceCollection, List<Assembly>? assemblyList)
    {
        if (assemblyList == null) return;
        DynamicProxyClass.ScanningComponentAttribute(serviceCollection, assemblyList);
    }

    /// <summary>
    /// 替换服务提供者
    /// </summary>
    /// <param name="hostBuilder"></param>
    public static void ReplaceServiceProvider(this IHostBuilder hostBuilder)
    {
        hostBuilder.UseServiceProviderFactory(new AutowiredServiceProviderFactory());
    }

}