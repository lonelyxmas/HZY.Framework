﻿namespace HZY.Framework.DependencyInjection.Attributes;

/// <summary>
/// 服务注册组件特性
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
public class ComponentAttribute : Attribute
{
    /// <summary>
    /// 服务生命周期
    /// </summary>
    public ServiceLifetime ServiceLifetime { get; set; } = ServiceLifetime.Transient;

    /// <summary>
    /// 实现类接口类型
    /// </summary>
    public Type? InterfaceType { get; set; }

    /// <summary>
    /// 服务注册组件特性
    /// </summary>
    public ComponentAttribute()
    {

    }

    /// <summary>
    /// 服务注册组件特性
    /// </summary>
    public ComponentAttribute(ServiceLifetime serviceLifetime)
    {
        ServiceLifetime = serviceLifetime;
    }

    /// <summary>
    /// 服务注册组件特性
    /// </summary>
    /// <param name="interfaceType"></param>
    public ComponentAttribute(Type interfaceType)
    {
        InterfaceType = interfaceType;
    }

    /// <summary>
    /// 服务注册组件特性
    /// </summary>
    public ComponentAttribute(Type interfaceType, ServiceLifetime serviceLifetime)
    {
        InterfaceType = interfaceType;
        ServiceLifetime = serviceLifetime;
    }

}

#if NET8_0

/// <summary>
/// 服务注册组件特性
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
public class ComponentAttribute<TInterface> : ComponentAttribute where TInterface : class
{
    /// <summary>
    /// 服务注册组件特性
    /// </summary>
    public ComponentAttribute() : base(typeof(TInterface))
    {

    }

    /// <summary>
    /// 服务注册组件特性
    /// </summary>
    /// <param name="serviceLifetime"></param>
    public ComponentAttribute(ServiceLifetime serviceLifetime) : base(typeof(TInterface), serviceLifetime)
    {

    }
}

#endif