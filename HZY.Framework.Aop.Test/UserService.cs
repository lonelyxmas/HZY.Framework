﻿using HZY.Framework.Aop.Attributes;

namespace HZY.Framework.Aop.Test;

public class UserService : IAopServiceProvider
{
    public IServiceProvider ServiceProvider { get; set; }

    public UserService(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
    }

    [Time("获取名称")]
    public string GetName()
    {
        return "好的。。。";
    }



}
