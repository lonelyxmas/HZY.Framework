﻿namespace HZY.Framework.Aop;

/// <summary>
/// 服务提供者
/// </summary>
public interface IAopServiceProvider
{
    /// <summary>
    /// 服务提供者
    /// </summary>
    IServiceProvider ServiceProvider { get; set; }
}
